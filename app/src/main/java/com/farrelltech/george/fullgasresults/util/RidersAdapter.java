package com.farrelltech.george.fullgasresults.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farrelltech.george.fullgasresults.R;

import java.util.List;

public class RidersAdapter extends RecyclerView.Adapter<RidersAdapter.MyViewHolder> {

    private List<Riders> ridersList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, position, club;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.main_title);
            club = (TextView) view.findViewById(R.id.main_rating);
            position = (TextView) view.findViewById(R.id.main_year);
        }
    }


    public RidersAdapter(Context applicationContext, List<Riders> ridersList) {
        this.ridersList = ridersList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.singleitem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Riders riders = ridersList.get(position);
        holder.title.setText(riders.getName());
        holder.club.setText(riders.getClub());
        holder.position.setText(riders.getFinishPosition());
    }

    @Override
    public int getItemCount() {
        return ridersList.size();
    }
}