package com.farrelltech.george.fullgasresults.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farrelltech.george.fullgasresults.R;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> {

    private List<Events> eventsList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, position, club;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.main_title);
            club = (TextView) view.findViewById(R.id.main_rating);
            position = (TextView) view.findViewById(R.id.main_year);
        }
    }


    public EventsAdapter(Context applicationContext, List<Events> eventsList) {
        this.eventsList = eventsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.eventsitem, parent, false);

        return new MyViewHolder(itemView);
    }
    

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Events events = eventsList.get(position);
        holder.title.setText(events.getName());
        holder.club.setText(events.getCreated());
        holder.position.setText(events.getUpdated());
    }

    @Override
    public int getItemCount() {
        return eventsList.size();
    }
}