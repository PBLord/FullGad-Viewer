package com.farrelltech.george.fullgasresults;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.farrelltech.george.fullgasresults.util.Riders;
import com.farrelltech.george.fullgasresults.util.RidersAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ResultsActivity extends AppCompatActivity {

    public String url = "http://farrelltech.org/Fullgas/Results/results_test_json.php";

    private RecyclerView rlist;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<Riders> ridersList;
    private RecyclerView.Adapter adapter;

    public static Intent makeIntent(Context context) {
        return new Intent(context, ResultsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        rlist = findViewById(R.id.recycler_view);
        ridersList = new ArrayList<>();
        adapter = new RidersAdapter(getApplicationContext(),ridersList);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(rlist.getContext(),linearLayoutManager.getOrientation());

        rlist.setHasFixedSize(true);
        rlist.setLayoutManager(linearLayoutManager);
        rlist.addItemDecoration(dividerItemDecoration);
        rlist.setAdapter(adapter);

        getData();
    }

    private void getData(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading results...");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++){
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        Riders riders = new Riders();
                        riders.setName(jsonObject.getString("Full_Name"));
                        riders.setClub(jsonObject.getString("Club"));
                        riders.setFinishPosition(jsonObject.getString("Finish_Position"));

                        ridersList.add(riders);
                        progressDialog.dismiss();
                    } catch (JSONException e){
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }
}
