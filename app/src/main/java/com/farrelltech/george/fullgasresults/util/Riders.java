package com.farrelltech.george.fullgasresults.util;

public class Riders {
    public String name;
    public String club;
    public String finishPosition;

    public Riders() {
        this.name = name;
        this.club = club;
        this.finishPosition = finishPosition;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public void setFinishPosition(String finishPosition) {
        this.finishPosition = finishPosition;
    }

    public String getName() {
        return name;
    }

    public String getClub() {
        return club;
    }

    public String getFinishPosition() {
        return finishPosition;
    }
}