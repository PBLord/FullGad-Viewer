package com.farrelltech.george.fullgasresults;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ResultsHome extends AppCompatActivity {

    public static Intent makeIntent(Context context) {
        return new Intent(context, ResultsHome.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_home);
    }

    public void resultsPress(View view){
        Intent intent = ResultsActivity.makeIntent(ResultsHome.this);
        startActivity(intent);
    }
}
