package com.farrelltech.george.fullgasresults.util;

public class Events {
    public String name;
    private String created;
    private String updated;

    public Events() {
        this.name = name;
        this.created = created;
        this.updated = updated;
    }

    public String getName() {
        return name;
    }

    public String getCreated() {
        return created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }
}