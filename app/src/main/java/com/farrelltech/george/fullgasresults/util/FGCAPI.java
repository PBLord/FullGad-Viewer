package com.farrelltech.george.fullgasresults.util;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by Lord on 12/03/2018.
 */

public interface FGCAPI {

    String BASE_URL = "wwww.reddit.com";

    @Headers("Content-type: application/json")
    @GET(".json")
    Call<Feed> getFeed();
}
